package wk02Collections;

import java.util.*;

public class collectionAssignClass {

    public static void main(String[] args) {
// Start List Example Code
        List<String> fj = new ArrayList<>();
        System.out.println("- Terrible First Jobs List -");
        fj.add("Bagger");
        fj.add("Intern");
        fj.add("Food Runner");
        fj.add("Secretary");
// Sort the output in ascending order.
        Collections.sort(fj);

        for (Object str : fj) {
            System.out.println((String) str);
        }
// Start code for TreeSet
        System.out.println(" ");
        System.out.println("- Current Staff Titles Set -");
        Set sTitles = new TreeSet();
        sTitles.add("Manager");
        sTitles.add("Clerk");
        sTitles.add("Associate");
        sTitles.add("Manager");
        sTitles.add("Utility Clerk");
        sTitles.add("Receptionist");



        for (Object str : sTitles) {
            System.out.println((String) str);
        }

// Start Queue Example Code
        Queue welcome = new PriorityQueue();
        welcome.add("Please do not browse personal sites.");
        welcome.add(" ");
        welcome.add("The proxy server will log all website data.");

        System.out.println("\n");
        System.out.println("- Network Introduction -");
        Iterator iterator = welcome.iterator();
        while (iterator.hasNext()) {
            System.out.println(welcome.poll());
        }

// Start an additional Tree Set Example
        System.out.println("\n");
        System.out.println("- Favorite Beverages -");
        TreeSet<String> favoriteDrinks = new TreeSet<String>();

        favoriteDrinks.add("Pepsi");
        favoriteDrinks.add("Red Bull");
        favoriteDrinks.add("Snapple");
        favoriteDrinks.add("Gatorade");

        // Duplicate tree entries will not be added
        favoriteDrinks.add("Snapple");

        System.out.println(" ");
        System.out.println(favoriteDrinks);
// Using a Hash Set Example
        System.out.println("\n");
        System.out.println("- I'm a Child of God Verses -");
        Set<String> hash_Set
                = new HashSet<String>();

        hash_Set.add("I am a child of God,");
        hash_Set.add("And he has sent me here,");
        hash_Set.add("Has given me an earthly home");
        hash_Set.add("With parents kind and dear.");
        hash_Set.add("Lead me, guide me, walk beside me,");
        hash_Set.add("Help me find the way.");
        hash_Set.add("Teach me all that I must do");
        hash_Set.add("To live with him someday.");

        System.out.println(hash_Set);


    }
}
